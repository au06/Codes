//Closure and lexical env
function main() {
    let a = 10
    let b = 20
    sum()
    function sum() {
        let total = a+b
        ans()
        function ans() {
            console.log(a)
            console.log(b)
            console.log(total)
            }
    }
    return sum;
}
let z = main()
console.log(z)
z()
