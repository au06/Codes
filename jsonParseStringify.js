//JSON Parse Stringify
let jsonString ='{ "name":"John", "age":30, "city":"New York"}'
var obj = { name: "John", age: 30, city: "New York" };

// JSON parse
let jsonObj = JSON.parse(jsonString)
console.log(typeof jsonString)
console.log(typeof jsonObj)

// JSON Stringify
let str= JSON.stringify(obj)
console.log(typeof str)
console.log(typeof obj)
