/* Write a regex pattern that matches the password
           Uppercase (A-Z) and lowercase (a-z) English letters.
           Digits (0-9).
           Characters ! # $ % & ' * + - / = ? ^ _ ` { | } ~
           Character. ( period, dot or full stop) provided that it is not the first or last character and it will not come one after the other. */
           
let pass = prompt("Input Password to validate","Password")
let regex = /^[^\.](?!.*?\.\.)([!#$%&'*+-\/=/?/^`|\{\}\w\.]+)[^\.]$/
if(regex.test(pass)) {
    console.log(`${pass} is a valid password`)
} else {
     console.log(`${pass} is not a valid password`)
}
