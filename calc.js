/* Create a constructor function Calculator that creates objects with 3 methods:
        read() asks for two values using prompt and remembers them in object properties.
        sum() returns the sum of these properties.
        mul() returns the multiplication product of these properties.
*/
function Calculator(a,b) {
    this.a=a;
    this.b=b;
    this.read = function() {
        this.a = prompt("Give value for","a");
        this.b = prompt("Give value for","b");
        console.log(`Given inputs are ${this.a},${this.b}`)
    };
    this.sum =function() {
        console.log(typeof this.a,typeof this.b)
        let add = Number(this.a)+Number(this.b)
        return add
    };
    this.mul =function() {
        let product = this.a*this.b
        return product
    }
    
}
let calc = new Calculator()
calc.read()
console.log(calc.sum())
console.log(calc.mul())

