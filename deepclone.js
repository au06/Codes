/* Deep clone Javascript Object (without using any internal methods of cloning). All properties along with functions, prototypes should get cloned to target objects. */
let obj1 = {name : {fname : "Aman", lname: "Upadhyay"}, 
        Address : { House : "B-14",City : "Gwalior", State : "MP"},
        Contact : 8602684605,
        get fullDetail (){
            console.log("\"Name = "+this.name.fname +" "+ this.name.lname+"\""+" "+"\"Contact = "+this.Contact+"\""+" \"Address "+this.Address.House+" "+this.Address.City+" "+this.Address.State+"\"")}
            }
            
// let obj2=JSON.parse(JSON.stringify(obj1))
// obj2.name.fname = "ty"
// console.log(obj1)
// console.log(obj2)

console.log(Object.keys(obj1))
console.log(Object(obj1))
