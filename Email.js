//Write a regex pattern to match the valid email address
           
let email =prompt("Input an email to validate","Email")
let regex = /^([\w])(?!.*\.\.)([\w\.!#$%&'*+-\/=/?/^`|\{\}]{3,64})@([a-z]{2,8})(\.[a-z]{2,8})(\.[a-z]{2,8})?$/
if(regex.test(email)) {
    console.log(`${email} is a valid email`)
} else {
     console.log(`${email} is not a valid email`)
}
