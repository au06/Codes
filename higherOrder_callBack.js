//Higher order function and callback function

array = [25,45,65,7,12,9,29,5]

function arrOper(arr,operation) {
    let  result = []
    for (let i = 0; i < arr.length;i++){
    result.push(operation(arr[i]))
    }
    return result
} 
function square(element) {
    return element * element
    }

console.log(arrOper(array,square))
