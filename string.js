//String
let str = " The quick brown fox jumps over the lazy dog "

//trim
console.log(str.trim(" "))

//slice
console.log(str.slice(4,20))

//substring
console.log(str.substring(17,1))

//split
console.log(str.split(' '))

//repeat
console.log(str.repeat(5))

//charAt
console.log(str.charAt(19))
