//Implement the setTimeout function using native javascript only.


function inTime() {
    console.log("Function executed first but inside setTimeOut")
}
function outTime() {
    console.log("Function executed later but outside setTimeOut")
}

setTimeout(inTime,5000)
outTime()
