"use strict";
let name1 = {
    fname : "Aman",
    lname : "Upadhyay"
}
let name2 = {
    fname : "Micheal",
    lname : "Smith"
}
let name3 = {
    fname : "Scarlet",
    lname : "wallberg"
}
function printName(gender, home) {
    console.log(this.fname + " " + this.lname +" "+ gender+" "+home)
}
//Call
printName.call(name1,"Male","Gwalior")
//Apply
printName.apply(name2,["Male","Canada"])
//Bind
let print = printName.bind(name3,"Female","Europe")
print()
