/* Give the reasons for the output the above snippet gives. Also, modify the snippet to print values from 0 to 9. 
Here is a code snippet: */

for(let i = 0; i < 10; i++) {
    
   setTimeout(function() {
       
     console.log(i); 

   },10);

}
/* Reason : as we are using i as var to instead to value of i on each iteration newly assigned inside to settimeout itbeing used as reference when using let the block new updated value of is stored inside settimeout with in the event queue as  the settimeout function wait before executing the function in its body and during that time the forloop has been executed by increasing the value  of i 0-9 and on while exiting at i=9 it incremented ti by 1 and failed to enter durig next execution

