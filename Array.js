let array = [78,11,98,75,23,75,98,2,43,90,43,1,6,1,6,92,2,67,54,11,86]


//using for each
let unique =[]
array.forEach(function (element,index) {
    if(unique.indexOf(element) == -1)
    unique.push(element)
})
console.log(unique)

//using filter
let unique = array.filter(function (element,index){
     return array.indexOf(element) == index   
})
console.log( unique)
 
//using Reduce
let unique = [];
array.reduce(function (def,element) {
    if(unique.includes(def) == false)
    unique.push(def)
    
    if(unique.includes(element) == false)
    unique.push(element) 
     
    return element        
})
console.log(unique)

//Using set
let unique = [...new Set (array)]
console.log(unique)
