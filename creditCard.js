//Write a regex pattern that verifies credit card pattern

           
let name = prompt("Input Card holder name","Name")
let card_no = prompt("Input card number","Card number")
let cvv = prompt("Input CVV secutity number","CVV")
let datem = prompt("Input Validity month","Date")
let datey = prompt("Input Validity year","Date")
let nameReg = /^[a-zA-z\s]{3,25}$/
let cardNumReg = /^[0-9]{16}$/
let cvvReg = /^[0-9A-Z]{3,4}$/
let datemReg = /^([a-zA-Z]{3}|[0|1][0-9])$/
let dateyReg = /^[1|2][0-9]$/

if(!nameReg.test(name)) {
    console.log("Invalid Cardholder Name")
} else if(!cardNumReg.test(card_no)) {
    console.log("Invalid Card number")
} else if(!cvvReg.test(cvv)) {
    console.log("Invalid Cvv")
} else if(!datemReg.test(datem)) {
    console.log("Invalid Month")
} else if(!dateyReg.test(datey)) {
    console.log("Invalid Year")
} else {
    console.log("Card is Valid")
}
