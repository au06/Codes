/*Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order. If the Array contains anything but Strings, it should throw an error. */

let array = ["wefAQWD","qqwOLwS","ssddsf","dfdfgwfsd","rtdvwqw","dsfERG","Sdeff","ADWDW","rtyhrh"]

 let makeAllCaps = (array) => new Promise((resolve,reject) => {
    if(array.every(string => ((typeof string) === "string"))) {
        let arrayUpper = array.map(string => string.toUpperCase())
        resolve(arrayUpper)
            }
            else {
                reject(Error("Invalid String"))
                }
                })
                
let sortWords = (array) => new Promise((resolve,reject) =>{
    if((Array.isArray(array))) {
        resolve(array)
    }else
    {
        reject(Error("Passed Array is not valid"))
    }
})


makeAllCaps(array)
  .then(arrayUpper => sortWords(arrayUpper))
  .then(array => console.log(array))
  .catch(error => console.log(error))
