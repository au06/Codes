var fs = require('fs')
var jobsJSON 
var technologiesJSON 
var tagged

jobsJSON = JSON.parse(fs.readFileSync('jobs.json',{encoding:"utf8"}))
        
technologiesJSON = JSON.parse(fs.readFileSync('technologies.json',{encoding:"utf8"}))

function tag(){
  
    for(let jobs in jobsJSON) {
        let job = jobsJSON[jobs].description

        for(let techs in technologiesJSON) {
            let tech = technologiesJSON[techs]

             if(job.includes(tech)) {
                 
                 if(("tags" in jobsJSON[jobs])) {

                    jobsJSON[jobs].tags.push(tech)

                 } else {
                     
                       jobsJSON[jobs].tags = [tech]
                 }
                 jobsJSON[jobs].processing_timestamp=Date.now()
            }
        }
    }
    tagged = JSON.stringify(jobsJSON)

    fs.writeFile(`${Date.now()}_response.json`,tagged, error => {
        if (error) {
        console.error(error);
        return;
        }
    })
}
 
tag()
