const readline = require("readline-sync")
var cart = [{}]
var i = 0

function cartCreator() { 
    let flag = null
    temp = readline.question("Input Detail : Quantity Description Price ")

    cart[i].quantity  = temp.substr(0,temp.indexOf(' '))
    cart[i].desc = temp.substring(temp.indexOf(' '),temp.lastIndexOf('at'))
    cart[i].price =  temp.substr(temp.lastIndexOf(' '))

    if(temp.includes("choclate")||temp.includes("drink")||temp.includes("milk")||temp.includes("apple")||temp.includes("mange")||temp.includes("chips")||temp.includes("cookie")||temp.includes("cereal")) {
        cart[i].category = "food"
    }
    else if(temp.includes("medicine")||temp.includes("pill")||temp.includes("syrup")) {
        cart[i].category = "medicine"
    }
    else if(temp.includes("book")||temp.includes("guide")||temp.includes("manual")) {
        cart[i].category = "book"
    }
    else{
        cart[i].category = "other"
    }
    if(temp.includes("imported")) {
        cart[i].imported = true
    } else {
        cart[i].imported = false
    }

    flag = readline.question("Input Y to add more item and N to save cart ")
    if(flag == 'Y' || flag == 'y') {
        i++
        cart.push({})
        cartCreator(cart)
    }
    else {

        console.log("Cart Updated Successfully")
        for(i in cart)
        {
            console.log(`${cart[i].quantity}x${cart[i].desc}@${cart[i].price} `)
        }
        console.log("Calculating Cart Value")
        cartCalc(cart)
    }

}
function cartCalc() {
    let salesTax = 0

    for(item in cart) {
        if((cart[item].category == "other") && (cart[item].imported == true)) {
            salesTax += cart[item].price * 1.15 * cart[item].quantity
            cart[item].price *= 1.15 * cart[item].quantity
        } else if ((cart[item].category != "other") && (cart[item].imported == true)) {
            salesTax += cart[item].price * 1.05 * cart[item].quantity
            cart[item].price *= 1.05 * cart[item].quantity
        } else {
            salesTax += cart[item].price * 1.10 * cart[item].quantity
            cart[item].price *= 1.10 * cart[item].quantity
        }
    }
    console.log("Cart Value with Sales Tax included :")
    for(item in cart) {
        console.log(`${cart[item].quantity}${cart[item].desc}: ${cart[item].price} `)
    }
    console.log(`Sales Tax = ${salesTax}`)
}

cartCreator()