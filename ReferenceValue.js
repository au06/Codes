//Pass by value

let a=10;
function val(c) {
    c++
    console.log(c)
}
val(a)
console.log(a)

//Pass by reference

let obj={a : 10}
function ref(b) {
    b.a = 12
}
ref(obj)
console.log(obj)
