/* Provided a function that checks the validity of string and returns results via a callback.
Check if values in array (see below example) are valid or not.
Example
Input: ['first', 'Second', 'thiRd', 4, false, 'true']
Output: {"4":false,"first":true,"Second":false,"thiRd":false,"false":false,"true":true}
NOTE: you can not use loops or recursion. Also, you should not change the ‘validateString’ functio (i.e. use it as it is). */
let array = ['first', 'Second', 'thiRd', 4, false, 'true']
function validateString(input, callback) {
setTimeout(function () {
    // input is said to be valid if it is a lowercase string
 
    if (typeof input === "string" && input === input.toLowerCase()) {

      return callback(null, true)
    }
    return callback(new Error('Invalid string'), null)
  }, 500)
}

let len = array.length
let i = 0;
let output = {}
function traverse(x,bool) {
    let input=array[i]
     if (bool === true) {
            console.log(bool,input)
        output[input] = true 
        } else {
            console.log(bool,input)
              output[input] = false
               }
                i++  
               input=array[i]
  if(i< len) {
      validateString(input,traverse)
      }  
      else {
            console.log(output)
        }
}
validateString(array[0],traverse)
